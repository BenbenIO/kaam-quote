# kaam-quote

An [`oh-my-zsh`](https://github.com/robbyrussell/oh-my-zsh) plugin, that display a random quote taken from Kaamelott (Karadoc / Perceval / Kadoc).
The quote were taken from [this web-site](https://fr.wikiquote.org/wiki/Kaamelott)

# Installation

Clone this repository in `oh-my-zsh`'s plugins directory:
```bash
git clone https://gitlab.com/BenbenIO/kaam-quote.git ~/.oh-my-zsh/custom/plugins/kaam-quote
```

Add the plugin to the plugins array in your zshrc file and restart zsh:
```zsh
plugins=(... kaam-quote)
```
# Usage

* A random quote will be display when opening a new session.
* Run `kaam` to get a new random quote.

# To Do

* Better formattage and extraction of the quotes (e.g: delete too long quote, )
* Sentimental analysis on the quotes / ...
